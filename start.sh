#!/bin/bash

cd /opt/kafka_${KAFKA_VER} 
bin/zookeeper-server-start.sh config/zookeeper.properties &
sleep 10
bin/kafka-server-start.sh config/server.properties &
sleep inf