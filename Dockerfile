FROM openjdk:17-oracle

USER root

ARG VER "3.4.0"
ENV KAFKA_VER "2.12-3.4.0"

COPY start.sh /

WORKDIR /tmp
RUN chmod +x /start.sh && \
    curl https://downloads.apache.org/kafka/${VER}/kafka_${KAFKA_VER}.tgz -o ./kafka_${KAFKA_VER}.tgz
RUN tar -xf kafka_${KAFKA_VER}.tgz -C /opt && \
    cd /opt/kafka_${KAFKA_VER} 

COPY server.properties /opt/kafka_${KAFKA_VER}/config/server.properties
COPY zookeeper.properties /opt/kafka_${KAFKA_VER}/config/zookeeper.properties

CMD [ "/start.sh" ] 

